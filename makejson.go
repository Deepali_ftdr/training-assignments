package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"
)

func main() {
	fileReader := bufio.NewReader(os.Stdin)
	fmt.Println("Enter a Name:-")
	name, err := fileReader.ReadString('\n')
	if err != nil {
		log.Fatal(err.Error())
		return
	}
	name = strings.TrimRight(name, "\r\n")
	fmt.Println("Enter an Address:-")
	address, err := fileReader.ReadString('\n')
	if err != nil {
		log.Fatal(err.Error())
		return
	}
	address = strings.TrimRight(address, "\r\n")

	mapObj := map[string]string{}
	mapObj["name"] = name
	mapObj["address"] = address
	jsonObj, err := json.Marshal(mapObj)
	if err != nil {
		log.Fatal(err.Error())
		return
	}
	fmt.Println(string(jsonObj))
}
