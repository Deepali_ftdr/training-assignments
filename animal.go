package main

import "fmt"

type Animal struct {
	food       string
	locomotion string
	noise      string
}

func main() {
	animals := make(map[string]Animal)
	animals["cow"] = Animal{food: "grass", locomotion: "walk", noise: "moo"}
	animals["bird"] = Animal{food: "worms", locomotion: "fly", noise: "peep"}
	animals["snake"] = Animal{food: "mice", locomotion: "slither", noise: "hsss"}
	fmt.Println("first string either [“cow”, “bird”, or “snake”] and second strind either [“eat”, “move”, or “speak”] Respectively ")
	for {
		var name, data string
		fmt.Println(">")
		fmt.Scanf("%s %s", &name, &data)
		animal, err := animals[name]
		if !err {
			break
		}
		switch data {
		case "eat":
			animal.Eat()
		case "move":
			animal.Move()
		case "speak":
			animal.Speak()

		}
	}
}

func (a *Animal) Eat() {
	fmt.Println(a.food)
}
func (a *Animal) Move() {
	fmt.Println(a.locomotion)
}
func (a *Animal) Speak() {
	fmt.Println(a.noise)
}
