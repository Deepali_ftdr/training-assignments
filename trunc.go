package main

import (
	"fmt"
	"math"
)

func main() {
	truncate()
}
func truncate() {
	var floatNumber float64
	fmt.Println("Enter Float Number: ")
	fmt.Scanln(&floatNumber)

	fmt.Println("\t \t Using Type Casting :-")
	var truncated int = int(floatNumber)
	fmt.Printf("input number = %.4f\n", floatNumber)
	fmt.Printf("Convert flot to int = %d\n", truncated)

	fmt.Println("\t \t Using Truncate Funcation:- ")
	result := math.Trunc(floatNumber)
	fmt.Printf("input number = %.4f\n", floatNumber)
	fmt.Println("Convert flot to int =", result)
}
