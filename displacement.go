package main

import "fmt"

func main() {
	acceleration := inputFloat64("acceleration")
	initialVelocity := inputFloat64("initial velocity")
	initialDisplacement := inputFloat64("initial Displacement")
	computesDisplacement := GenDisplaceFn(acceleration, initialVelocity, initialDisplacement)
	time := inputFloat64("time")
	displacement := computesDisplacement(time)
	fmt.Println("Result :-", displacement)
}
func GenDisplaceFn(acceleration, initialVelocity, initialDisplacement float64) func(time float64) float64 {
	return func(time float64) float64 {
		return 0.5*acceleration*time*time + initialVelocity*time + initialDisplacement
	}
}

func inputFloat64(value string) float64 {
	var no float64
	for {
		fmt.Print("Enter a float64 value for ", value, ": ")
		var err error
		if _, err = fmt.Scanln(&no); err == nil {
			break
		} else {
			fmt.Println("Error..", err)
		}
	}
	return no
}
