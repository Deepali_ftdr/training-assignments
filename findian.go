package main

import (
	"fmt"
)

func main() {
	findian()
}

func findian() {
	var str string
	println("Enter a string:")
	fmt.Scan(&str)
	strLength := len(str)
	if strLength < 3 {
		fmt.Println("Not Found!")
		return
	}
	isFirstChar := str[0] == 105 || str[0] == (105-32)
	isEndChar := (str[strLength-1] == 110) || (str[strLength-1] == (110 - 32))
	isValid := isFirstChar && isEndChar
	for i := 1; i < strLength-1; i++ {
		if str[i] == 97 || str[i] == (97-32) {
			if isValid {
				fmt.Println("Found!")
				return
			}
		}
	}
	fmt.Println("Not Found!")
	return
}
