package main

import (
	"context"
	"fmt"
	"log"
	"sort"
	"strings"

	"go.mongodb.org/mongo-driver/bson"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var collection *mongo.Collection
var ctx = context.TODO()

type StudentSort struct {
	FirstName   string
	LastName    string
	RollNO      int32
	Age         int32
	Marks       int32
	Gender      string
	AddressData Address
}
type Address struct {
	city    string
	zipcode int64
	state   string
	country string
}

type byAge []StudentSort

func (p byAge) Len() int {
	return len(p)
}

func (p byAge) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}

func (p byAge) Less(i, j int) bool {
	return p[i].Age > p[j].Age
}

type byMarks []StudentSort

func (p byMarks) Len() int {
	return len(p)
}

func (p byMarks) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}

func (p byMarks) Less(i, j int) bool {
	return p[i].Marks < p[j].Marks
}

type byFirstName []StudentSort

func (p byFirstName) Len() int {
	return len(p)
}

func (p byFirstName) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}

func (p byFirstName) Less(i, j int) bool {
	a, b := p[i].FirstName, p[j].FirstName
	return strings.ToLower(a) < strings.ToLower(b)
}

func main() {
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017/")
	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		log.Fatal(err)
	}

	err = client.Ping(ctx, nil)
	if err != nil {
		log.Fatal(err)
	}
	collection = client.Database("student-data").Collection("student")

	var results []StudentSort
	cur, err := collection.Find(context.TODO(), bson.D{}, options.Find())
	if err != nil {
		log.Fatal(err)
	}

	for cur.Next(context.TODO()) {
		var elem StudentSort
		err := cur.Decode(&elem)
		if err != nil {
			log.Fatal(err)
		}

		results = append(results, elem)
		fmt.Printf("First Name:%v", elem.FirstName)
		fmt.Println()
		fmt.Printf("Last Name:%v", elem.LastName)
		fmt.Println()
		fmt.Printf("Roll No:%v", elem.RollNO)
		fmt.Println()
		fmt.Printf("Age:%v", elem.Age)
		fmt.Println()
		fmt.Printf("Marks:%v", elem.Marks)
		fmt.Println()
		fmt.Printf("Gender:%v", elem.Gender)
		fmt.Println()
		fmt.Printf("City:%v", elem.AddressData.city)
		fmt.Println()
		fmt.Printf("Zipcode:%v", elem.AddressData.zipcode)
		fmt.Println()
		fmt.Printf("State:%v", elem.AddressData.state)
		fmt.Println()
		fmt.Printf("Country:%v", elem.AddressData.country)
		fmt.Println()
		fmt.Println("________________________________")

	}
	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}

	cur.Close(context.TODO())
	fmt.Println("Sort record by age Descending order")
	sort.Sort(byAge(results))
	fmt.Println(results)

	fmt.Println("Sort record by marks Accending order")
	sort.Sort(byMarks(results))
	fmt.Println(results)

	fmt.Println("Sort record by FirstName Accending order")
	sort.Sort(byFirstName(results))
	fmt.Println(results)

}
