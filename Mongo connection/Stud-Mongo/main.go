package main

import (
	"context"
	"fmt"
	"log"
	"sort"
	"strings"

	"go.mongodb.org/mongo-driver/bson"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Student struct {
	firstName string
	lastName  string
	rollNO    int32
	age       int32
	marks     int32
	gender    string
	address   Address
}
type Address struct {
	city    string
	zipcode int64
	state   string
	country string
}

var collection *mongo.Collection
var ctx = context.TODO()

func main() {

	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017/")
	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		log.Fatal(err)
	}

	err = client.Ping(ctx, nil)
	if err != nil {
		log.Fatal(err)
	}
	collection = client.Database("student-data").Collection("student")

	var results []Student
	cur, err := collection.Find(context.TODO(), bson.D{}, options.Find())
	if err != nil {
		log.Fatal(err)
	}

	for cur.Next(context.TODO()) {
		var elem Student
		err := cur.Decode(&elem)
		if err != nil {
			log.Fatal(err)
		}

		results = append(results, elem)
	}

	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}

	cur.Close(context.TODO())
	fmt.Printf("\nStudent Records : %+v\n", results)
	fmt.Println()

	firstNameSorted := make(firstna, 0, len(results))
	for _, d := range results {
		firstNameSorted = append(firstNameSorted, d)
	}
	sort.Sort(firstNameSorted)
	fmt.Println("Sorted result :", firstNameSorted)

}

type firstna []Student

func (p firstna) Len() int {
	return len(p)
}

func (p firstna) Less(i, j int) bool {
	//return p[i].firstName < p[j].firstName
	a, b := p[i].firstName, p[j].firstName
	return strings.ToLower(a) < strings.ToLower(b)
}

func (p firstna) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}
