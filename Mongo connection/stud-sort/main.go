package main

import (
	"fmt"
	"quickstart/stud-sort/models"
)

func main() {
	var studModel models.StudentModel
	fmt.Println("Sort Age Asc order")
	stud, err := studModel.SortAgeASC()
	if err != nil {
		fmt.Println(err)
	} else {
		for _, s := range stud {
			fmt.Println(s.ToString())
			fmt.Println("---------------------------")
		}
	}
	fmt.Println("Sort Age Desc order")
	stud, err1 := studModel.SortAgeDesc()
	if err1 != nil {
		fmt.Println(err1)
	} else {
		for _, s := range stud {
			fmt.Println(s.ToString())
			fmt.Println("---------------------------")
		}
	}
	fmt.Println("Sort LastName Asc order")
	stud, err2 := studModel.SortLastNameAsc()
	if err2 != nil {
		fmt.Println(err2)
	} else {
		for _, s := range stud {
			fmt.Println(s.ToString())
			fmt.Println("---------------------------")
		}
	}
	fmt.Println("Sort City Dsec order")
	stud, err3 := studModel.SortCityDesc()
	if err3 != nil {
		fmt.Println(err3)
	} else {
		for _, s := range stud {
			fmt.Println(s.ToString())
			fmt.Println("---------------------------")
		}
	}
}
