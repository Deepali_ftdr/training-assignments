package config

import (
	"fmt"

	"github.com/globalsign/mgo"
)

func GetMongoDB() (*mgo.Database, error) {
	host := "mongodb://localhost:27017"
	dbName := "student-data"
	session, err := mgo.Dial(host)
	if err != nil {
		fmt.Println("Error while connecting to DB..........")
		return nil, err
	}
	db := session.DB(dbName)
	fmt.Printf("\nDataBase Name: %s", dbName)
	fmt.Println("\nConnecting to DB.....!!!!!!!!!!!")
	return db, nil
}
