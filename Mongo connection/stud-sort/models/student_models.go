package models

import (
	"fmt"
	"quickstart/stud-sort/config"
	"quickstart/stud-sort/entities"

	"go.mongodb.org/mongo-driver/bson"
)

type StudentModel struct {
}

func (this StudentModel) SortAgeASC() ([]entities.Student, error) {
	db, err := config.GetMongoDB()
	if err != nil {
		return nil, err
	} else {
		var stud []entities.Student
		//Age Asc order sort using mongo sort()
		err2 := db.C("student").Find(bson.M{}).Sort("age").Iter().All(&stud)
		if err2 != nil {
			fmt.Printf("Not connecting to DB")
			return nil, err
		} else {
			fmt.Printf("connect to DB Connections.....")
			return stud, nil
		}
	}
}
func (this StudentModel) SortAgeDesc() ([]entities.Student, error) {
	db, err := config.GetMongoDB()
	if err != nil {
		return nil, err
	} else {
		var stud []entities.Student
		//Age Desc order sort using mongo sort()
		err2 := db.C("student").Find(bson.M{}).Sort("-age").Iter().All(&stud)
		if err2 != nil {
			fmt.Printf("Not connecting to DB")
			return nil, err
		} else {
			fmt.Printf("connect to DB Connections.....")
			return stud, nil
		}
	}
}

func (this StudentModel) SortLastNameAsc() ([]entities.Student, error) {
	db, err := config.GetMongoDB()
	if err != nil {
		return nil, err
	} else {
		var stud []entities.Student
		//Lastname  Asc order sort using mongo sort()
		err2 := db.C("student").Find(bson.M{}).Sort("lastName").All(&stud)
		if err2 != nil {
			fmt.Printf("Not connecting to DB")
			return nil, err
		} else {
			fmt.Printf("connect to DB Connections.....")
			return stud, nil
		}
	}
}

func (this StudentModel) SortCityDesc() ([]entities.Student, error) {
	db, err := config.GetMongoDB()
	if err != nil {
		return nil, err
	} else {
		var stud []entities.Student
		//city  Desc order sort using mongo sort()
		err2 := db.C("student").Find(bson.M{}).Sort("-city").Iter().All(&stud)
		if err2 != nil {
			fmt.Printf("Not connecting to DB")
			return nil, err
		} else {
			fmt.Printf("connect to DB Connections.....")
			return stud, nil
		}
	}
}
