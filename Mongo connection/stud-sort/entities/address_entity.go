package entities

import (
	"fmt"
)

type AddressStud struct {
	City    string `bson:"city"`
	Zipcode int64  `bson:"zipcode"`
	State   string `bson:"state"`
	Country string `bson:"country"`
}

func (this AddressStud) ToString() string {
	addre := fmt.Sprintf("\ncity: %s", this.City)
	addre = addre + fmt.Sprintf("\nzipcode: %d", this.Zipcode)
	addre = addre + fmt.Sprintf("\nstate: %s", this.State)
	addre = addre + fmt.Sprintf("\ncountry: %s", this.Country)
	return addre
}
