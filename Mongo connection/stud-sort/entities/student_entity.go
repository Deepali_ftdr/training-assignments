package entities

import (
	"fmt"
)

type Student struct {
	FirstName   string      `bson:"firstname"`
	LastName    string      `bson:"lastName"`
	RollNO      int32       `bson:"rollNo"`
	Age         int32       `bson:"age"`
	Marks       int32       `bson:"marks"`
	Gender      string      `bson:"gender"`
	AddressData AddressStud `bson:"address"`
}

func (this Student) ToString() string {
	result := fmt.Sprintf("\nfirstName: %s", this.FirstName)
	result = result + fmt.Sprintf("\nlastName: %s", this.LastName)
	result = result + fmt.Sprintf("\nRoll No: %d", this.RollNO)
	result = result + fmt.Sprintf("\nage: %d", this.Age)
	result = result + fmt.Sprintf("\nmarks: %d", this.Marks)
	result = result + fmt.Sprintf("\ngender: %s", this.Gender)
	result = result + this.AddressData.ToString()
	return result
}
