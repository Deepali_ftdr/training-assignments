package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

type name struct {
	fname string
	lname string
}

func main() {
	var fileName string
	nameSlice := make([]name, 0, 20)
	fmt.Println("Enter a file name:-")
	fmt.Scan(&fileName)
	file, err := os.Open(fileName)
	if err != nil {
		log.Fatal(err.Error())
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		words := strings.Fields(line)
		nameSlice = append(nameSlice, name{words[0], words[1]})
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err.Error())
		return
	}
	for _, v := range nameSlice {
		fmt.Println(v.fname, v.lname)
	}

}
