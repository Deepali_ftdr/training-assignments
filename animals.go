package main

import (
	"errors"
	"fmt"
)

type Animal interface {
	Eat()
	Move()
	Speak()
	AnimalName
}
type Cow struct{ name string }
type Bird struct{ name string }
type Snake struct{ name string }

type AnimalName interface{ getName() string }

//get name of all animals
func (cow Cow) getName() string {
	return cow.name
}
func (bird Bird) getName() string {
	return bird.name
}
func (snake Snake) getName() string {
	return snake.name
}

//Eat() method for all animals
func (cow Cow) Eat() {
	fmt.Println("grass")
}
func (bird Bird) Eat() {
	fmt.Println("worms")
}
func (snake Snake) Eat() {
	fmt.Println("mice")
}

//Move() method for all animals
func (cow Cow) Move() {
	fmt.Println("walk")
}
func (bird Bird) Move() {
	fmt.Println("fly")
}
func (snake Snake) Move() {
	fmt.Println("slither")
}

//Speak() method for all animals
func (cow Cow) Speak() {
	fmt.Println("moo")
}
func (bird Bird) Speak() {
	fmt.Println("peep")
}
func (snake Snake) Speak() {
	fmt.Println("hsss")
}

func getInformation() (string, string, string) {
	var command, name, option string
	for {
		fmt.Println(">")
		if _, err := fmt.Scanln(&command, &name, &option); err != nil {
			fmt.Println("Error:-", err.Error())
		} else {
			if checckCommand(command) {
				if command == "newanimal" {
					if checkAnimal(option) {
						break
					} else {
						fmt.Println("Please enter correct animal type either:cow,bird,snake")
					}

				} else if command == "query" {
					if checkAnimalProperty(option) {
						break
					} else {
						fmt.Println("Please enter correct animal property either:eat,move,speak")
					}
				}
			} else {
				fmt.Println("Please enter either newanimal or query type as input")
			}
		}

	}
	return command, name, option
}

func checkAnimalProperty(animalProperty string) bool {
	return animalProperty == "eat" || animalProperty == "move" || animalProperty == "speak"
}

func checkAnimal(animal string) bool {
	return animal == "cow" || animal == "bird" || animal == "snake"
}

func checckCommand(command string) bool {
	return command == "newanimal" || command == "query"
}
func searchAnimal(a []Animal, name string) (Animal, error) {
	var serach Animal
	err := errors.New("Animal name not found")
	for _, animal := range a {
		if AnimalName(animal).getName() == name {
			serach = animal
			err = nil
			break
		}
	}
	return serach, err
}
func addAnimal(animals *[]Animal, animal Animal) {
	*animals = append(*animals, animal)
	fmt.Println("Created it!")
}
func main() {
	var animals []Animal
	fmt.Println("Enter command('newanimal'or 'query'), name, option() parameter as input string:-")
	for {
		command, name, option := getInformation()
		switch command {
		case "newanimal":
			switch option {
			case "cow":
				addAnimal(&animals, Cow{name})
			case "bird":
				addAnimal(&animals, Bird{name})
			case "snake":
				addAnimal(&animals, Snake{name})

			}
		case "query":
			var animal Animal
			var err error
			if animal, err = searchAnimal(animals, name); err != nil {
				fmt.Println(err.Error())
				break
			}
			switch option {
			case "eat":
				animal.Eat()
			case "move":
				animal.Move()
			case "speak":
				animal.Speak()
			}
		}
	}
}

//OutPut

/* C:\Users\deepali.kokate\go\src\course2-week4>go run animals.go
Enter command('newanimal'or 'query'), name, option() parameter as input string:-
>
newanimal Bessie cow
Created it!
>
newanimal parat bird
Created it!
>
newanimal cobra snake
Created it!
>
query Bessie eat
grass
>
query Bessie move
walk
>
query Bessie speak
moo
>
query parat eat
worms
>
query parat move
fly
>
query parat speak
peep
>
query cobra eat
mice
>
query cobra move
slither
>
query cobra speak
hsss
> */
