package main

import (
	"fmt"
	"sort"
	"strconv"
)

func main() {
	initSize := make([]int, 3)
	var inputValue string
	count := 0
	for {
		fmt.Printf("Enter Integer Input :-")
		fmt.Scan(&inputValue)

		if inputValue == "X" || inputValue == "x" {
			fmt.Println("Input Character is ‘X’ then program is quit")
			break
		}

		if result, err := strconv.Atoi(inputValue); err == nil {
			if count < 3 {
				initSize[count] = result
			} else {
				initSize = append(initSize, result)
			}
			sortSlice := make([]int, len(initSize))
			copy(sortSlice, initSize)
			sort.Ints(sortSlice)
			fmt.Println(sortSlice)
			count++
		}

	}
}
