package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func main() {
	slice := ReadSliceData()
	Bubblesort(slice)
	fmt.Println(slice)
}

func swap(slice []int, i int) {
	slice[i], slice[i+1] = slice[i+1], slice[i]
}
func Bubblesort(slice []int) {
	for i := 1; i < len(slice); i++ {
		for j := 0; j < len(slice)-1; j++ {
			if slice[j] > slice[j+1] {
				swap(slice, j)
			}
		}
	}
}

func ReadSliceData() []int {
	sc := bufio.NewScanner(os.Stdin)
	fmt.Println("Enter a size of array between 0 to 10 :-")
	sc.Scan()
	size, err := strconv.Atoi(sc.Text())
	if err != nil || size > 10 || size < 0 {
		log.Fatal("Array size is incorrect", err.Error())
	}
	fmt.Println("Enter an array to sort:- ")
	value := make([]int, size)
	for i := 0; i < size; i++ {
		sc.Scan()
		data, err := strconv.Atoi(sc.Text())
		if err != nil {
			log.Fatal("Input value is incorrect:-(Press enter after each element of the array)")
		}
		value[i] = data
	}
	return value
}
