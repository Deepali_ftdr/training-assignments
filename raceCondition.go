package main

import (
	"fmt"
	"time"
)

var globalVarible int

func main() {

	go func() {
		globalVarible = 3
		globalVarible = globalVarible + 1
		fmt.Println("Increment Function is excuted..>>")
	}()
	go func() {
		fmt.Println("Global Variable:", globalVarible)
	}()
	time.Sleep(50 * time.Millisecond)
}

//explanation of race conditions

/* A race condition exists between the following 2 goroutines due to the shared variable `globalVarible`.
It can occur when 'globalVarible' is printed from the second goroutine when the first one is still calculating the
final value
If we run the application multiple times, it will sometimes print 0 and sometimes 4.
My test were showing that majority of times it would print 0 but sometimes it would print 4.

*/
